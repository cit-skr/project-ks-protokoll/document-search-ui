FROM node:lts

ENV NPM_CONFIG_LOGLEVEL warn


RUN mkdir /app
WORKDIR /app

COPY package*.json ./

RUN npm install

RUN npm audit fix --force

EXPOSE 3000

ENTRYPOINT [ ]