import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { ApolloClient, ApolloProvider, InMemoryCache, HttpLink, gql } from '@apollo/client';
import { CookiesProvider } from 'react-cookie';

// Create a WebSocket link:
const link = new HttpLink({
  uri: "http://ec2-13-49-27-116.eu-north-1.compute.amazonaws.com:5000/v1/graphql",
  options: {
    reconnect: true,
    connectionParams: {
      headers: {
        "content-type":"application/json"
      }
    }
  }
});
const cache = new InMemoryCache();
const client = new ApolloClient({
  link,
  cache
});

ReactDOM.render(
  <React.StrictMode>
    <CookiesProvider>
      <ApolloProvider client={client}> 
          <App />
      </ApolloProvider>
    </CookiesProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
