const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = function (app) {
  app.use(
    createProxyMiddleware("/bert/", { target: "http://localhost:6000/" }),
    createProxyMiddleware("/api/", { target: "http://localhost:5000/" })
  );
};