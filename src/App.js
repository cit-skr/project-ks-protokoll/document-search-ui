import React, { useState, useEffect } from 'react';
import { makeStyles, useTheme, withStyles } from '@material-ui/core/styles';

import { Router, Switch, Route, Redirect } from "react-router-dom";
import {CssBaseline, Container, Typography, Grid, Toolbar, AppBar, IconButton, Button, GridList, GridListTile, Tab, Tabs, Box} from "@material-ui/core"
import { useMutation, useQuery } from '@apollo/client';

import { useCookies, Cookies} from 'react-cookie';

import history from './history';
import Navbar from "./search/Navbar"
import SearchResults from "./search/SearchResults"
import LandingPage from "./search/LandingPage"

import Classifier from "./common/Classifier"


import SettingsForm from "./common/SettingsPage"
import {RecursiveTreeView} from "./file_handling/FileManager"
import {getSearchSuggestions, getSearchResults, getPredictions} from "./common/BackendServices"
import { GET_MUNICIPALITY_NAMES } from './common/HasuraQueries';

import {searchSettings} from "./common/config.json"

import LoginPage from "./auth/LoginPage"

import ProtectedRoute from "./auth/ProtectedRoute"
import { Skeleton } from '@material-ui/lab';





function App() {
  const [isAuthenticated, setIsAuthenticated ] = useState(false);
  const [inputValue, setInputValue] = useState('');
  const [inputText, setInputText] = useState('');
  const [searchSuggestions, setSearchSuggestions] = useState([]);
  const [loadingSearchResults, setLoadingSearchResults] = useState(false);

  const [predictions, setPredictions] = useState(null);
  const [loadingPredictions, setLoadingPredictions] = useState(false);

  const [radioGroupValue, setRadioGroupValue] = useState('bert');
  const [searchResults, setSearchResults] = useState([]);
  const [openSettingsDialog, setOpenSettingsDialog] = useState(false);
  const [searchResultsCount, setSearchResultsCount] = useState(searchSettings.nResults);
  const [hitsCount, setHitsCount] = useState(searchSettings.nHits);
  const [showPredictions, setShowPredictions] = useState(false);

  const [kommunList, setKommunList] = useState([]);
  const { loading, error, data } = useQuery(GET_MUNICIPALITY_NAMES);

  const [cookies, setCookie] = useCookies("authenticated");

 

  useEffect( ()=>{
    if (!loading && data) {
        const j = data.ks_protokoll_schema_municipality_info.map((item,_)=> {
        return {
            id:item.municipality_id,
            kommun:item.name,
            region: item.county_name
        };
        });
        setKommunList(j);
        var cookies = new Cookies()
        if (cookies.get('nSearchResults')) setSearchResultsCount(cookies.get('nSearchResults'));
        if (cookies.get("nHits")) setHitsCount(cookies.get("nHits"));
    }
  }, [loading, data]);

  function onKeyPress(event) {
    const event_value = event.target.value;
    var inp = String.fromCharCode(event.which);
    if (/[a-zA-ZäöåÄÖÅ0-9-_ ]/.test(inp)){
      // console.log(event_value)
      if (event_value.length > 3) {
        getSearchSuggestions(event_value, setSearchSuggestions)
        var words = event_value.split(' ')
        if (words.length>1) {
          // words.map((v,_)=>{getSearchSuggestions(v, setSearchSuggestions);})
        }
      }
      return true;
    }
    if (event.which === 13 || event.keyCode === 13) {
      // event.preventDefault()
      // if (event_value.length >= 0) {
        setLoadingSearchResults(true);
        getSearchResults(event_value,setSearchResults,setLoadingSearchResults, searchResultsCount, hitsCount)
        setInputValue(event_value);
        history.push('/search');
        
        // console.log(searchResults);
        // console.log(kommunList)

      // }
      return true;
    } 
    
  }

  function onComputeButtonClick() {
    // setInputText('')
    setLoadingPredictions(true);
    getPredictions(inputText, setPredictions, setLoadingPredictions);
    setShowPredictions(true);
  }

  function changeInput(e) {
    setInputText(e.target.value);
  }

  function onRadioButtonClick(e) {
    setRadioGroupValue(e.target.value);
    return true;
  }

  function onListItemClick(item) {
    // Note: use url created from file path rather than original url!!
    window.open(item, "_blank");
  }

  function handleSettingsDialogClose() {
    setCookie("nSearchResults", searchResultsCount, {path: '/'})
    setCookie("nHits", hitsCount, {path: '/'})
    setOpenSettingsDialog(false);
  }

  const handleOpenSettingsDialog = () => {
    setOpenSettingsDialog(true);
  }

  const login = (rememberPassword) => {
    setIsAuthenticated(true);
    if(rememberPassword) setCookie("authenticated", "yes", {path: '/'});
    history.push("/");
  }

  const logout = () => {
    setIsAuthenticated(false);
    setCookie("authenticated", "no", {path: '/'})
    history.push("/login");
  }

  const [tabValue, setTabValue] = useState(0);
  const handleTabChange = (event, newValue) => {
    setTabValue(newValue);
  };

  const classifyIconClick = (text) => {
    // var k = ""
    // k.re
    var t = text.replaceAll("<b>", "").replaceAll("</b>","");
    setInputText(t);
    // onComputeButtonClick();
    setShowPredictions(false);
    history.push("/classifier");
  };

  return (
    <>
      <CssBaseline/>
      <Router history={history}>

        
          
        <Switch>
          <Route path="/login">
            <LoginPage
              login={login}
            />
          </Route>
          
          <ProtectedRoute exact path="/"
            isAuthenticated={isAuthenticated}
            component={
                <LandingPage
                  inputValue={inputValue}
                  options={searchSuggestions}
                  radioGroupValue = {radioGroupValue}
                  logout={logout}
                  onKeyPress={e => onKeyPress(e)}
                  onRadioButtonClick={e=>onRadioButtonClick(e)}
                  handleOpenSettingsDialog = {handleOpenSettingsDialog}
                />
            }
          />
      

          
          <ProtectedRoute exact path="/search"
            isAuthenticated={isAuthenticated}
            component={
              <>
                <Navbar 
                  inputValue={inputValue}
                  options={searchSuggestions}
                  onInputKeyPress={e => onKeyPress(e)}
                  searchBarDisplay='flex'
                  avatarDisplay='none'
                  logout={logout}
                  handleOpenSettingsDialog = {handleOpenSettingsDialog}
                />
      
                <SearchResults
                  searchResults={searchResults}
                  inputValue={inputValue}
                  kommunList={kommunList}
                  loadingSearchResults={loadingSearchResults}
                  onListItemClick={onListItemClick}
                  tabValue={tabValue}
                  handleTabChange={handleTabChange}
                  classifyIconClick={classifyIconClick}
                />

              </>

              
            }
          />

          <ProtectedRoute exact path="/classifier"
            isAuthenticated={isAuthenticated}
            component={
              <>
                <Navbar 
                  inputValue={inputValue}
                  options={searchSuggestions}
                  onInputKeyPress={e => onKeyPress(e)}
                  searchBarDisplay='none'
                  avatarDisplay='flex'
                  logout={logout}
                  handleOpenSettingsDialog = {handleOpenSettingsDialog}
                />
                <Container maxWidth="md">
                  <Classifier
                    inputText={inputText}
                    predictions={predictions}
                    loadingPredictions={loadingPredictions}
                    onInputChange={(e)=> changeInput(e)}
                    onButtonClick={(e) => onComputeButtonClick(e)}
                    showPredictions={showPredictions}
                  />
                </Container>
              </>
            }
          />

          <ProtectedRoute exact path="/fileManager"
            isAuthenticated={isAuthenticated}
            component={
              <>
                <Navbar 
                  inputValue={inputValue}
                  options={searchSuggestions}
                  onInputKeyPress={e => onKeyPress(e)}
                  searchBarDisplay='none'
                  avatarDisplay='flex'
                  logout={logout}
                  handleOpenSettingsDialog = {handleOpenSettingsDialog}
                />
                <Grid container style={{overflow:"hidden"}}  >
                    <RecursiveTreeView
                      kommunList={kommunList}
                    />
                </Grid>
              </>
            }
          />
            
        </Switch>

        
      </Router>

      <SettingsForm
        openSettingsDialog={openSettingsDialog}
        searchResultsCount={searchResultsCount}
        hitsCount={hitsCount}
        setSearchResultsCount={setSearchResultsCount}
        setHitsCount={setHitsCount}
        handleSettingsDialogClose={handleSettingsDialogClose}
      />
      </>

  );
}

export default App;



const sample_preds = [
  {
    name: 'Information', likelihood: 10,
  },
  {
    name: 'Decision', likelihood: 10,
  },
  {
    name: 'Cirkulär', likelihood: 10,
  }
];