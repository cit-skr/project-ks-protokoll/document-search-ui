import { gql, useQuery } from '@apollo/client';

const INSERT_SENTENCE = gql`
    mutation insert_sentence($file_id: Int! , $municipality_id: Int! , $label: String! , $text: String!, $search_query: String!, $label_id: Int! ) {
        insert_ks_protokoll_schema_extracted_sentences_one(object: {file_id: $file_id, municipality_id: $municipality_id, label: $label, text: $text, search_query: $search_query, label_id:$label_id}) {
            sentence_id
            file_id
            municipality_id
            label
            label_id
            text
            search_query
        }
    }
`

const INSERT_LABEL = gql`
    mutation insert_label ($label: String!) {
        insert_ks_protokoll_schema_sentence_labels_one(object: {label: $label}) {
            id
            label
        }
    }
`

const GET_LABELS = gql`
    query MyQuery {
        ks_protokoll_schema_sentence_labels {
            id
            label
        }
    }
`

const GET_MUNICIPALITY_NAMES = gql`
    query MyQuery {
        ks_protokoll_schema_municipality_info(distinct_on: name) {
            municipality_id
            code
            name
            county_name
        }
    }
`

const INSERT_FILE_INFO = gql`
    mutation file_insert($pdf_url: String!, $municipality_id: Int! ) {
        insert_ks_protokoll_schema_file_info_one(object: {pdf_url: $pdf_url, content_type: "application/pdf", is_indexed: "no", download_status: "todo", municipality_id: $municipality_id}) {
            file_id
        }
    }
`

const sentenceLabels = [
    "Information",
    "Situation assessement",
    "Pre-decision",
    "Decision" ,
    "Cirkulär",
    "Överenskommelse"
];



export {
    INSERT_SENTENCE,
    INSERT_LABEL,
    GET_LABELS,
    GET_MUNICIPALITY_NAMES,
    sentenceLabels,
    INSERT_FILE_INFO
}
