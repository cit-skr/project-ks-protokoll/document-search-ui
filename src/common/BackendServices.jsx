import React, { memo, useState, useEffect } from 'react';
import axios from "axios";


export const getSearchSuggestions = (event_value, setSuggestions) => {
    const response = fetch("/api/elasticsearch/next_sentence", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({"input": event_value})
      }).then(res => res.json()).then(data => {
        setSuggestions(data.suggestions)
      });
}

export const getSearchResults = (event_value, setSearchResults, setLoadingSearchResults, searchResultsCount, hitsCount) => {
    const response = fetch("/api/elasticsearch/query", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({input: event_value, nResults: searchResultsCount, nHits: hitsCount })
      }).then(res => res.json()).then(data => {
        // console.log(data)
        setSearchResults(data.search_results);
        setLoadingSearchResults(false);
    });
}


export const getPredictions = (inputText, setPredictions, setLoadingPredictions) => {
    const response = fetch("/bert/sentence_class", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({"input": inputText})
      }).then(res => res.json()).then(data => {
        // console.log(data)
        setPredictions(data.predictions)
        setLoadingPredictions(false);
      }).catch((error)=>{
        setLoadingPredictions(false);
      });
  
      if (response.ok) {
        console.log("response worked!");
      }
}


export const startClassifierTraining = (setTrainingProgress) => {
  const response = fetch("/bert/classifier/train", {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },
    }).then(res => res.json()).then(data => {
      console.log(data.status);
      setTrainingProgress({
        inProgress:  (data.status=="ok")?true:false,
        progress:0,
        info: String(data.status)
      });

    });

    if (response.ok) {
      console.log("response worked!");
    }
}

export const getClassifierTrainingProgress = (setTrainingProgress) => {
  const response = fetch("/bert/classifier/train/status", {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    }).then(res => res.json()).then(data => {
      var progress = Number(data.train_status["progress"]);
      setTrainingProgress({
          inProgress: progress<100?true:false,
          progress:progress,
          info: `accuracy: ${data.train_status.accuracy}, loss: ${data.train_status.loss}`
      });
    });

    if (response.ok) {
      console.log("response worked!");
    }
}



export const getDirs = (setDirs) => {
    var dirs = []
    const response = fetch("/api/folder_structure", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
    }).then(res => res.json()).then(data => {
        // console.log(data.dirs)
        setDirs(data.dirs);
    });
    // console.log(response)
    if (response.ok) {
        console.log("response worked!");
    }
    return dirs
    
}

export const uploadFiles = (files, metaData, callback)=> {
    if (files.length) {
      files.map(async (item,index) => {
            const formPayload = new FormData()
            formPayload.append(`${index.toString()} ${metaData}`, item.file)
            try {
                await axios({
                    // baseURL: 'http://localhost:5000',
                    url: '/api/upload/files',
                    method: 'post',
                    data: formPayload,
                    onUploadProgress: (progress ) => {
                        const { loaded, total } = progress;
                        const percentageProgress = Math.floor((loaded / total) * 100);
                        callback({status: "progress", text: item.text, progress: percentageProgress,id: index });
                    },
                })
                callback({status: "success", text: item.text, progress: 100, id: index});
            } catch (error) {
                callback({status: "failure", text: item.text, progress: 0, id: index});
            }
      });
    }
} 


export const uploadUrls = (urls, path, m_id) => {

  urls.forEach((url, index) => {
 
      const response = fetch("/api/upload/url", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({url:url,path:path, m_id: m_id})
      }).then(res => res.json()).then(data => {
        console.log(data)
      });
  });
  
}


export const downloadFiles = (path, setFileUrl) => {
    
    axios({
        url: "/api/download?path=" + path,
        method: 'POST',
        responseType: 'blob', // important
      }).then((response) => {
      
        var fileType = path.endsWith(".pdf")? 'application/pdf': null
        const url = window.URL.createObjectURL(new Blob([response.data],{type: fileType }));
        setFileUrl(url)
      });
}


export const deleteFile = (path,size,setDeleteStatus) => {
    fetch("/api/delete", {
        method: 'POST',
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({"path": path, "size": size})
      }).then(res => res.json()).then(data => {
        console.log(data)
        setDeleteStatus(data)
      });
}

export const renameFileFolder = (path, newName) => {
    const response = fetch("/api/rename", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({"path": path, "name": newName})
      }).then(res => res.json()).then(data => {
        console.log(data)
      });
    return true;
}

export const createFolder = (path, newName) => {
  const response = fetch("/api/create_folder", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({"path": path, "name": newName})
    }).then(res => res.json()).then(data => {
      console.log(data)
    });
  return true;
}


function _uploadFiles(files, metaData) {
    var form_data = new FormData();
    files.map((item,index)=> {
        form_data.append(`${index.toString()} ${metaData}`, item.file)
    })

    const response = fetch("/api/upload", {
        method: "POST",
        body: form_data
        }).then(res => res.json()).then(data => {
        console.log(data)
    });
    console.log(response)
    if (response.ok) {
        console.log("response worked!");
    }
    
}