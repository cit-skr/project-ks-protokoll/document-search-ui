import React, { memo, useState, useEffect } from 'react';
import { TextField, Dialog, DialogTitle , DialogActions, DialogContent, DialogContentText, IconButton, LinearProgressWithLabel, CircularProgress, LinearProgress, Box } from "@material-ui/core";
import { Button, Divider, Typography} from "@material-ui/core";

import { useMutation, useQuery } from '@apollo/client';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import PauseCircleFilledIcon from '@material-ui/icons/PauseCircleFilled';

import {getClassifierTrainingProgress, startClassifierTraining} from "./BackendServices"


const StartStopProgressBar = (props) => {
    const [openConfirmDialog, setOpenConfirmDialog] = useState(false);

    function LinearProgressWithLabel(props) {
        return (
          <Box display="flex" alignItems="center">
            <Box width="100%" mr={1}>
              <LinearProgress variant="determinate" {...props} />
            </Box>
            <Box minWidth={35}>
              <Typography variant="body2" color="textSecondary">{`${Math.round(
                props.value,
              )}%`}</Typography>
            </Box>
          </Box>
        );
    };

    return (
        <>
            <Typography> 
                {props.title}
                <IconButton edge="end" aria-label="start-stop-progress" onClick={() => setOpenConfirmDialog(true)} disabled={props.disabled}>
                    {props.inProgress? < PauseCircleFilledIcon/> : < PlayCircleFilledIcon/>}
                </IconButton>
                {props.inProgress?<CircularProgress size={24} style={{marginBottom:-8, marginLeft:-24}} color="primary" />: null}
            </Typography>
            {props.inProgress?<LinearProgressWithLabel variant="determinate" value={props.progress} />:null}
            <Typography variant="caption" style={{marginTop:16}}> 
                {props.info}
            </Typography>

            <Dialog
                open={openConfirmDialog}
                onClose={()=>setOpenConfirmDialog(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Start process?"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Are you sure to start the process?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={()=>setOpenConfirmDialog(false)} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={props.onStart} color="primary" autoFocus>
                        Confirm
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};



const SettingsForm = memo((props) => {

    const [trainProgress, setTrainProgress] = useState({
        inProgress:false,
        progress:0,
        info:null
    });


    useEffect(() => {
        const timer = setTimeout(() => {
            if (trainProgress.inProgress){
                getClassifierTrainingProgress(setTrainProgress);
            }
        }, 1000);
        return () => clearTimeout(timer);
        
    });
  
    const onSave = () => {
        props.handleSettingsDialogClose()
    }

    const handleClickStartTraining = () => {
        if (!trainProgress.inProgress){
            startClassifierTraining(setTrainProgress);
        } else {
            alert("Not implemented! Cannot pause ^_^ ")
        }
        
    }

    return (
        <div>
            <Dialog open={props.openSettingsDialog} onClose={props.handleSettingsDialogClose} aria-labelledby="form-dialog-title" >
                <DialogTitle id="form-dialog-title">Change app settings</DialogTitle>
                <DialogContent style={{height:800,width:600}}>
                    <Typography style={{marginTop:16, marginBottom:16}}>
                        
                    </Typography>

                    <Divider/>
                    <DialogContentText style={{marginTop:16}}>
                        Search settings:
                    </DialogContentText>

                    <TextField
                        margin="dense"
                        variant="outlined"
                        color="primary"
                        id="search_results_count"
                        label="Results per search"
                        type="number"
                        value={props.searchResultsCount} 
                        onChange={(e) => props.setSearchResultsCount(e.target.value)} 
                        style={{marginRight:16}}
                    />

                    <TextField
                        margin="dense"
                        variant="outlined"
                        color="primary"
                        id="search_results_count"
                        label="hits per result"
                        type="number"
                        value={props.hitsCount} 
                        onChange={(e) => props.setHitsCount(e.target.value)} 
                    />


                    <Divider style={{marginTop:16, marginBottom:16}}/>

                    <StartStopProgressBar
                        title={"Train text classifier"}
                        progress={trainProgress.progress}
                        info={trainProgress.info}
                        inProgress={trainProgress.inProgress}
                        onStart={handleClickStartTraining}
                        disabled={false}
                    />

                    <Divider style={{marginTop:16, marginBottom:16}}/>

                    <StartStopProgressBar
                        title={"Download and parse urls "}
                        progress={50}
                        disabled={true}
                    />

                    <StartStopProgressBar
                        title={"Launch WebScraper"}
                        progress={100}
                        disabled={true}
                    />


                    
                    
                </DialogContent>

                <DialogActions>
                    <Button onClick={props.handleSettingsDialogClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={onSave} color="primary">
                        Save
                    </Button>
                </DialogActions>

            </Dialog> 

         
        </div>
    );
});

export default SettingsForm