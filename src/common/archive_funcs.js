import React from "react"

function call(event) {
    if (event.which === 32 || event.keyCode === 32 ) {
        if (radioGroupValue === 'bert') {
          const response = fetch("/bert/next_word", {
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify({"input": event.target.value})
          }).then(res => res.json()).then(data => {
            // console.log(data)
            setOptions(data.predictions)
          });
        }
        if (radioGroupValue === 'gpt2') {
          const response = fetch("/gpt2/next_sentence", {
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify({"input": event.target.value})
          }).then(res => res.json()).then(data => {
            // console.log(data)
            setOptions(data.predictions)
          });
        }
    
      }
    if (event.which === 9 || event.keyCode === 9 || event.code === 'Tab' ) {
        event.preventDefault();
        console.log(' tab ')
    }
    return false;
}


function filterDir(nested_list, id) {
  var dir = nested_list
  const findNested = (obj, parent, value, i) => {
    if (obj.id === value) {
        parent.splice(i,1)
    }
    if (obj && obj.children && obj.children.length > 0) {
        for (let j = 0; j < obj.children.length; j++) {
            findNested(obj.children[j], obj.children, value, j);
        }
    }
}
  for (let i = 0; i < dir.length; i++) {
    findNested(dir[i], dir, id, i); 
  }
  setDirs(dir)
}