import React, { memo } from 'react'
import TextField from '@material-ui/core/TextField';
import { Card, Container, Paper, Typography, IconButton, CircularProgress, Box } from '@material-ui/core';
import Grid from '@material-ui/core/Grid'
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button'
import {Send} from '@material-ui/icons'
import {
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import AddSentenceButton from '../search/AddSentenceButton'
import AssessmentIcon from '@material-ui/icons/Assessment';
import Plot from 'react-plotly.js';
import {displayText} from './config.json'

const getData = (data) => {
    console.log(data)
    console.log(Object.keys(data))
    console.log(Object.values(data))

    var trace = {
        x: Object.keys(data),
        y: Object.values(data),
        name: 'Likelihood',
        type: 'bar'
    }
    return [trace]
}

const Classifier = memo((props) => {
    return (
        <>
        <Paper elevation={2} style={{ margin: 16, padding: 16, }}>
               
            <Grid container spacing={3} alignItems="center" justify="center" style={{alignItems:"center", display:"flex", flexGrow:1}}>
                <Grid item xs={12}>
                    <Grid container alignItems="center">
                        <Grid item xs={8}>
                            <Typography variant="h5" style={{ paddingBottom: 16 }} > <b>{displayText.classifierPage.classifierLabel} </b> (BERT) </Typography>
                        </Grid>
                        <Grid item xs={4}>
                            <Typography variant="p" style={{ paddingBottom: 16 }} > 
                                {displayText.classifierPage.AddSentenceLabel} 
                                <AddSentenceButton 
                                    sentence={props.inputText}
                                    search_query={null}
                                    file_id={null}
                                    municipality_id={null}
                                />
                            </Typography>
                        </Grid>
                    </Grid>
                    
                    <TextField 
                        placeholder={displayText.classifierPage.textBoxPlaceholder}
                        value={props.inputText} 
                        onChange={props.onInputChange} 
                        variant="outlined" 
                        fullWidth
                        multiline
                        rows={4}
                        rowsMax={8}
                        style={{ margin: 0}}
                    />
                    
                </Grid>
                <Grid item justify="flex-end" alignContent="center">
                    
                    <Button variant="contained" color="secondary" onClick={props.onButtonClick} startIcon={<AssessmentIcon/>} >
                        {displayText.classifierPage.classifyButtonText}
                    </Button>
                    {/* <IconButton aria-label="compute-class" edge="end" onClick={props.onButtonClick} variant="contained">
                        <Send fontSize="default" />
                    </IconButton> */}
                    
                </Grid>

                
        
            </Grid>
       
        </Paper>
        
        {
            props.loadingPredictions &&
            <Box style={{ marginTop:48, justifyItems:"center", justifyContent:"center", display:"flex"}}>
                <CircularProgress color="secondary" />
            </Box>
        }
        
        <Paper  variant="elevation" style={{ marginTop:16, justifyItems:"center", justifyContent:"center", display:"flex"}}>
        
            
            
            {
                props.predictions && props.showPredictions && !props.loadingPredictions &&
                <Plot
                    data={ getData(props.predictions)}
                    layout={ {title: 'Likelihood', yaxis: {title: 'Probability', range: [0,100],}, xaxis: {title: 'label'}  }}
                />
            }

        </Paper>
        

        </>
    );
});

export default Classifier;

