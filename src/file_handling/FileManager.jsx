import React, {memo, useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';
import { Divider, Button, Grid, GridListTile, IconButton, List, ListItemSecondaryAction, ListSubheader, Paper, Tooltip, Typography, Toolbar, Container, Drawer, ListItem, CircularProgress } from '@material-ui/core';
import {TextField, Dialog, DialogTitle , DialogActions, DialogContent, DialogContentText,Box, Popover , Collapse, Fade} from "@material-ui/core";
import { resetApolloContext } from '@apollo/client';
import GridList from '@material-ui/core/GridList';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import CreateNewFolderIcon from '@material-ui/icons/CreateNewFolder';
import ErrorIcon from '@material-ui/icons/Error';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { Delete } from '@material-ui/icons';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

import UploadForm from "./UploadForm"
import {downloadFiles, deleteFile, getDirs, renameFileFolder, createFolder} from "../common/BackendServices"

const useStyles = makeStyles( (theme) => ({
  root: {
    height: "100%",
    width: "100%",
    display: 'flex',
    position:"relative",
    flexGrow: 1,
    padding:0, 
    marginBottom:6
  },
  view: {
    height: "100%",
    flexGrow: 3,
    marginLeft:16,
    marginTop:6,
    marginBottom:0,
    display: 'flex',
  }
}));

var sample_node = {
  "children": null, 
  "date": "Sun Oct 25 20:03:35 2020", 
  "id": "10", 
  "name": "2020-01-22.txt", 
  "path": "/path/to/file/folder", 
  "size": "74.1KB"
}

export const RecursiveTreeView = memo( (props) => {
  const classes = useStyles();
  const [dirs, setDirs] = useState({});
  const [fileUrl, setFileUrl] = useState(null);
  const [openUploadDialog, setOpenUploadDialog] = useState(false);
  const [node, setNode] = useState(null);
  const [enableEdit, setEnableEdit] = useState(true);
  const [enableDelete, setEnableDelete] = useState(true);
  const [enableUpload, setEnableUpload] = useState(true);
  const [deleteStatus, setDeleteStatus] = useState(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [openEditDialog, setOpenEditDialog] = useState(false);
  const [openCreateFolderDialog, setOpenCreateFolderDialog] = useState(false);
  const [newName, setNewName] = useState("");

  const [openFileProgess, setOpenFileProgress] = useState(false);
  const [fileProgress, setFileProgress] = useState([]);;
  
  var f = [];
  const setUploadProgress = (uploadStatus) => {
    f[uploadStatus.id] = {status: uploadStatus.status, progress: uploadStatus.progress, text: uploadStatus.text};
    setFileProgress(fileProgress.concat(f));
    if (uploadStatus.status === "success" || uploadStatus.status === "failure") {
      setOpenFileProgress(true);
    }
  }

  useEffect(() => {
    getDirs((d)=>(setDirs(d)));
  }, [dirs]);


  const fileViewer = (path) =>{
    downloadFiles(path, setFileUrl);
  }

  const manageFile = (node) => {
    setEnableUpload(true);
    setEnableEdit(false);
    setEnableDelete(false);
    setNode(node)
  }

  const manageFolder = (node) => {
    setEnableUpload(false);
    setEnableEdit(false);
    setEnableDelete(false);
    setNode(node)
  }

  const renderTree = (nodes) => (
    <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.name} onDoubleClick={()=> nodes.date? fileViewer(nodes.path): null} onLabelClick={()=> nodes.date? manageFile(nodes): manageFolder(nodes)}>
        {Array.isArray(nodes.children) ? nodes.children.map((node) => renderTree(node)) : null}
    </TreeItem>
  );

  return (
    <>
    <Grid container direction="row" alignItems="stretch"  >
      <Grid item xs={12} sm={2} style={{flexGrow:1, flexDirection:"row", minWidth:200}} component="div" onMouseLeave={()=>{setEnableDelete(true);setEnableEdit(true);setEnableUpload(true);}} >
          <Toolbar style={{ height: 'auto', display: "flex", flexGrow:1, justifyContent:"flex-end" }}>
              <Tooltip title="Create new folder">
                <IconButton aria-label="newfolder" edge="end" disabled={enableUpload} onClick={()=>setOpenCreateFolderDialog(true)}>
                    <CreateNewFolderIcon/>
                </IconButton>
              </Tooltip>  
              <Tooltip title="upload file(s)/url(s)">
                <IconButton aria-label="upload" edge="end" disabled={enableUpload} onClick={()=>setOpenUploadDialog(true)}>
                    <CloudUploadIcon/>
                </IconButton>
              </Tooltip>
              <Tooltip title="edit file/folder name">
                <IconButton aria-label="edit" edge="end"  disabled={enableEdit} onClick={()=>{setOpenEditDialog(true); setNewName(node.name)}}>
                    <EditIcon/>
                </IconButton>
              </Tooltip>
              <Tooltip title="delete files">
                <IconButton aria-label="delete" edge="end" disabled={enableDelete} onClick={()=>setOpenDeleteDialog(true)}>
                    <DeleteIcon/>
                </IconButton>
              </Tooltip>
         
          </Toolbar>
        <GridList cellHeight={"auto"} className={classes.root}>
          <Grid container style={{height:"86vh", overflow:"scroll"}} className={classes.view}>
            <TreeView
              // className={classes.view}
              defaultCollapseIcon={<ExpandMoreIcon />}
              defaultExpanded={['1']}
              defaultExpandIcon={<ChevronRightIcon />}
            >
              {renderTree(dirs)}
            </TreeView>

          </Grid>
          
        </GridList>

      </Grid>

      <Divider/>

      <Grid item xs={12} sm={10} component="div" >
        {
          fileUrl &&
          <iframe src={fileUrl} type="application/pdf" height="100%" width="100%" style={{position:"relative", display:"flex"}}></iframe>
        }        
      </Grid>
    </Grid>
    {
      node && openUploadDialog &&
      <UploadForm
        openDialog={openUploadDialog}
        setOpenFileProgress={setOpenFileProgress}
        setProgress={setUploadProgress}
        path={node.path}
        kommunList={props.kommunList}
        handleDialogClose={() => {setOpenUploadDialog(false); setDirs(dirs);} }
      />

    }
    
    {
      node &&
      <Dialog
          open={openDeleteDialog}
          onClose={()=>setOpenDeleteDialog(false)}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
      >
          <DialogTitle id="alert-dialog-title">{"Delete file?"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              You'll be deleting "{node.name}", do you want to continue?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>setOpenDeleteDialog(false)} color="primary">
              Cancel
            </Button>
            <Button onClick={()=>{deleteFile(node.path,node.size,setDeleteStatus);setOpenDeleteDialog(false);setDirs(dirs);}} color="primary" autoFocus>
              Confirm
            </Button>
          </DialogActions>
      </Dialog>
    }
    {
      node &&
      <Dialog open={openEditDialog} onClose={()=>setOpenEditDialog(false)} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Edit file/folder name</DialogTitle>
          <DialogContent>
            <DialogContentText>
              You'll be editing "{node.name}", do you want to continue?
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              value={newName}
              onChange={(e)=>setNewName(e.target.value)}
              type="text"
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>setOpenEditDialog(false)} color="primary">
              Cancel
            </Button>
            <Button onClick={(e)=>{ renameFileFolder(node.path,newName); setOpenEditDialog(false); setDirs(dirs)} } color="primary">
              Save
            </Button>
          </DialogActions>
      </Dialog>
    }
    {
      node &&
      <Dialog open={openCreateFolderDialog} onClose={()=>setOpenCreateFolderDialog(false)} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Create folder</DialogTitle>
          <DialogContent>
            <DialogContentText>
              You'll be adding a folder in {node.name}
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              value={newName}
              onChange={(e)=>setNewName(e.target.value)}
              type="text"
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>setOpenCreateFolderDialog(false)} color="primary">
              Cancel
            </Button>
            <Button onClick={(e)=>{ createFolder(node.path,newName); setOpenCreateFolderDialog(false); setDirs(dirs)} } color="primary">
              Create
            </Button>
          </DialogActions>
      </Dialog>
    }

    {
        fileProgress.length>0 &&
        <Popover open={openFileProgess} style={{ margin: 16 }} 
           anchorReference="anchorPosition" 
           anchorPosition={{ top: 750, left: 1100 }}
           anchorOrigin={{
           vertical: 'center',
           horizontal: 'left',
           }}
           transformOrigin={{
           vertical: 'bottom',
           horizontal: 'left',
           }}
          disableEnforceFocus
          disableScrollLock
          transitionDuration={"auto"}
          onClose={()=>setOpenFileProgress(false)}
       >

       <List style={{ overflow:"hidden" }}  
        subheader={
            <ListSubheader component="div" id="nested-list-subheader" >
              File upload progress
              <IconButton edge="end" aria-label="hide" style={{marginLeft:110}} onClick={() => setOpenFileProgress(false)} >
                <VisibilityOffIcon />
              </IconButton>
            </ListSubheader>
            
        }
       >
           { fileProgress.map((item, index)=>(
               <ListItem  key={index} divider={index !== fileProgress.length - 1} >
                   
                   {item.status==="success" ? <CheckCircleIcon style={{marginRight:6}} fontSize="large" /> : item.status=="failure" ? <ErrorIcon fontSize="large" style={{marginRight:6, color: "danger"}}/> : <></>}
           
                   { item.status==="progress" && 
                   
                       <Box position="relative" display="inline-flex">
                           <CircularProgress variant="static" size={36} value={item.progress} style={{marginRight:6}}/> 
                           <Box
                               top={0}
                               left={0}
                               bottom={0}
                               right={0}
                               position="absolute"
                               display="flex"
                               alignItems="center"
                               justifyContent="center"
                           >
                               <Typography variant="caption" component="div" color="textSecondary">{`${Math.round(
                               item.progress,
                               )}%`}</Typography>
                           </Box>
                       </Box>
                   
                   }
                   <Typography noWrap style={{width:200}}> {item.text}</Typography>
                   <IconButton edge="end" aria-label="remove" onClick={() => setFileProgress(fileProgress.filter(f=>f.text!==item.text))} >
                        <Delete />
                    </IconButton>
               </ListItem>
           ))}
       </List>     
       </Popover> 
    }
    </>
  );
});