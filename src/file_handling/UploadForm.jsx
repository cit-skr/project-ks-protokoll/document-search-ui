import React, { memo, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import {TextField, Dialog, DialogTitle , DialogActions, DialogContent, DialogContentText,Box, Popper , Collapse, Fade, Snackbar, ListItemIcon} from "@material-ui/core";
import { Button, Tooltip, IconButton, Grid, Paper, Divider, List, ListItem, ListItemText, ListItemSecondaryAction, Popover , Typography} from "@material-ui/core";
import Autocomplete from '@material-ui/lab/Autocomplete';
import CloseIcon from '@material-ui/icons/Close';
import ErrorIcon from '@material-ui/icons/Error';
import Fab from '@material-ui/core/Fab';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import SaveIcon from '@material-ui/icons/Save';
import CircularProgress from '@material-ui/core/CircularProgress';

import { useMutation, useQuery } from '@apollo/client';

import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import InsertLinkIcon from '@material-ui/icons/InsertLink';
import SendIcon from '@material-ui/icons/Send';
import { Delete, Edit } from '@material-ui/icons';

import { uploadFiles, uploadUrls } from "../common/BackendServices"

import {INSERT_FILE_INFO} from "../common/HasuraQueries"


const ItemList = (props) => {
    const [name, setName] = useState({new:"", old:""});
    const [openEditDialog, setOpenEditDialog] = useState(false);

    if (props.items.length>0 ){
        return (
            <>
            <Paper style={{ margin: 16 }}>
                <List style={{ overflow: "scroll" , maxHeight: 200}}>
                    {props.items.map((item, index) => (
                        <ListItem divider={index !== props.items.length - 1} key={index} role={undefined} dense autoFocus={true}>
                            <ListItemText primary={item.text}/>
                            <ListItemSecondaryAction>
                                <IconButton edge="end" aria-label="edit" onClick={() => {setOpenEditDialog(true); setName({new:item.text, old:item.text})}}>
                                    <Edit />
                                </IconButton>
                                <IconButton edge="end" aria-label="delete" onClick={() => props.onItemRemove(index)}>
                                    <Delete />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    ))}
                </List>
            </Paper>

            <Dialog open={openEditDialog} onClose={()=>setOpenEditDialog(false)} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Edit file name</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                    You'll be editing the file name.
                    </DialogContentText>
                    <TextField
                    autoFocus
                    margin="dense"
                    id="name"
                    value={name.new}
                    onChange={(e)=>setName({old: name.old, new:e.target.value})}
                    type="text"
                    fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={()=>setOpenEditDialog(false)} color="primary">
                    Cancel
                    </Button>
                    <Button onClick={(e)=>{ props.setFileName(name); setOpenEditDialog(false); } } color="primary">
                    Save
                    </Button>
                </DialogActions>
            </Dialog>
            </>
            
        );
    } else {
        return ( <> </> );
    }
    
};


const get_municipality_id = (path, data) => {
    for (var i=0; i < data.length; i++) {
        if ( path.includes(data[i].kommun) ) {
            return data[i].id;
        }
    }
    return null;
}


const UploadForm = memo((props) => {
    const [files, setFiles] = useState([]);
    const [url, setUrl] = useState("");
    const [urlList, setUrlList] = useState([]);

    const [insertFileInfo] = useMutation(INSERT_FILE_INFO);

    const updateFileList = (fileList, item) => {
        var out = [];
        fileList.map((fileItem,index) =>{
            if (fileItem.text === item.text) {
                out.push({...fileItem, ...item});
            } else {
                // out.push(fileItem);
            }
        })
        return out;
    };

    const onSubmit = () => {
        props.setOpenFileProgress(true);
        if (files.length>0)  {uploadFiles(files, props.path, props.setProgress)};
        if (urlList.length>0) {
            const m_id = get_municipality_id(String(props.path), props.kommunList )
            console.log(m_id)
            uploadUrls(urlList, props.path, m_id, insertFileInfo)
        }
    
        props.handleDialogClose()
        
    }

    const onEnter = (e) => {
        if (e.key == "Enter") {
            setUrlList([{text:url}].concat(urlList));
            setUrl("");
        }
    }

    const setFileName = (name) =>{
        setFiles(
            files.map((v,k)=>{
                if (v.text === name.old) {
                    v.text = name.new;
                }
                return v;
            })
        )
        
    }

    function onFileUpload(event) {
        event.preventDefault();
        const id = event.target.id;
        var file_list_temp = [];
        const files_object = event.target.files;

        Object.keys(files_object).map( (key,index)=>{
            file_list_temp.push({ text: files_object[key]["name"], file: files_object[key]});
        });
        setFiles(file_list_temp.concat(files));
    }
    return (
        <div>
            <Dialog open={props.openDialog} onClose={props.handleDialogClose} aria-labelledby="form-dialog-title" >
                <DialogTitle id="form-dialog-title">Upload file(s) or add url(s)</DialogTitle>
                <DialogContent style={{height:800,width:600}}>
                    <Typography style={{marginTop:16, marginBottom:16}}>
                        The file(s) or the url(s) will be added to <b>"{"downloaded_pdfs" + props.path.split('downloaded_pdfs')[1]}"</b>.
                    </Typography>
                    {/* <Autocomplete
                        id="combo-box-demo"
                        onSelect={(e)=>{setKommun(e.target.value)}}
                        options={kommunList.sort((a, b) => -b.region.localeCompare(a.region))}
                        groupBy={(option) => option.region}
                        getOptionLabel={(option) => `${String(option.id)} ${option.kommun}`}
                        style={{ width: 300 , marginBottom:16}}
                        renderInput={(params) => <TextField {...params} label="Choose kommun" variant="outlined" required />}
                    /> */}

                    <Divider/>
                    <DialogContentText style={{marginTop:16}}>
                        Add url(s)
                    </DialogContentText>

                    <TextField
                        margin="dense"
                        variant="outlined"
                        color="primary"
                        id="add_url"
                        placeholder="Enter url here..."
                        type="url"
                        value={url} 
                        onChange={(e) => setUrl(e.target.value)} 
                        onKeyPress={onEnter}
                        fullWidth
                    />

                    < ItemList
                        items={urlList}
                        onItemRemove={ (index) => 
                            setUrlList(
                                urlList.filter(
                                    (_,item_index)=> index!==item_index
                                )
                            )
                        }
                    />

                    <Divider style={{marginTop:16, marginBottom:16}}/>
                    <Grid container alignItems="flex-end" alignContent="center" direction="row" spacing={1}>
                        <Grid item >
                            <DialogContentText style={{marginTop:16}}>
                                Add files
                            </DialogContentText>
                        </Grid>

                        <Grid item >
                            <input
                                accept="*.pdf *.docx"
                                style={{display:"none"}}
                                id="contained-button-file"
                                onChange={(e)=>onFileUpload(e)}
                                multiple
                                type="file"
                            />
                            <label htmlFor="contained-button-file">
                                <Tooltip title="Add files for upload">
                                    <IconButton aria-label="Add files" edge="start" component="span">
                                        <CloudUploadIcon/>
                                    </IconButton>
                                </Tooltip>
                            </label>
                        </Grid>
                    </Grid>   

                    < ItemList
                        items={files}
                        setFileName={setFileName}
                        onItemRemove={ (index) => 
                            setFiles(
                                files.filter(
                                    (_,item_index)=> index!==item_index
                                )
                            )
                        }
                    />     
                    
                </DialogContent>

                <DialogActions>
                    <Button onClick={props.handleDialogClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={onSubmit} color="primary">
                        Submit
                    </Button>
                </DialogActions>

            </Dialog>

         
           
       

        </div>
    );
});

export default UploadForm;


 
