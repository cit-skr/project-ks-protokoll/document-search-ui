import React, {memo, useState} from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import {
   Tab, Tabs, Box, Toolbar, Container, Typography, Badge, Tooltip, IconButton, TextField
} from '@material-ui/core';
import TuneIcon from '@material-ui/icons/Tune';

import {ExportCSV} from "./DownloadSearchResults"
import ItemList from "./ItemList"

import Plot from 'react-plotly.js';
import { useEffect } from 'react';
import moment, { months } from "moment";
import { Autocomplete, Skeleton } from '@material-ui/lab';

import {displayText} from "../common/config.json"

const CustomTextField = withStyles({
  root: {
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderRadius: 15,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 6,
        padding:6,
      },
    },
  },
})(TextField);

const AntTabs = withStyles({
  root: {
    borderBottom: '1px solid #e8e8e8',
    marginLeft:"6%"
  },
  indicator: {
    backgroundColor: '#1890ff',
  },
})(Tabs);
  
const AntTab = withStyles((theme) => ({
  root: {
    textTransform: 'none',
    minWidth: 72,
    fontWeight: theme.typography.fontWeightRegular,
    marginRight: theme.spacing(4),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      color: '#40a9ff',
      opacity: 1,
    },
    '&$selected': {
      color: '#1890ff',
      fontWeight: theme.typography.fontWeightMedium,
    },
    '&:focus': {
      color: '#40a9ff',
    },
  },
  selected: {},
}))((props) => <Tab disableRipple {...props} />);

const SearchResults = memo((props) => {

  const [showFilters, setShowFilters] = useState(false);
  const [kommun, setKommun] = useState(null);

  // useEffect(()=>{
  //   console.log(props.searchResults)
  // },[props.searchResults])

  const getDate = (date) => {
    return date.slice(0,-2) + "01";  //moment(date).format("MMM 'YY")
  }

  const getDateRange = (months) => {
    var dt = new Date(Date.now());
    const cur_date = dt.toISOString().split('T')[0];
    dt.setMonth(dt.getMonth() - months);
    const old_date =  dt.toISOString().split('T')[0];
    const range = [old_date,cur_date];
    return range;

  }

  const getSentenceCount = (data) => {
    var totalSentences = 0;
    data.map((v,_)=>{
      totalSentences += v.items.length;
    })
    return totalSentences;
  }

  const getData = (data) => {
    // console.log(data);
    var counts = {};
    var date_counts = {}
    data.map((v, k) => {
      var region = v.path.split("/")[0];
      var dt = getDate(v.date);
      if ( !counts[region]) counts[region] = {};
      if (!counts[region][dt]) {
        counts[region][dt] = {count:0, score:v.score}
      } 
      counts[region][dt].count = 1 + (counts[region][dt].count || 0) ;
    })

    var dates = [];
    Object.keys(counts).map((k,_)=>{
      dates = dates.concat(Object.keys(counts[k]))
    });
    var dates_unique = Array.from(new Set(dates));
    var traces = []
    Object.keys(counts).map((k,_)=>{
      var info = counts[k];
      traces.push({
        x: dates_unique, 
        y:  dates_unique.map((v,_)=>{return info[v]?info[v].count:null;}), //Object.values(info).map((v,_) => {return v.count}), //
        name:k,
        type: "bar",
        // mode: 'markers',
        // text: Object.values(counts[k]).map((v,_)=>{return "docs: " +v.count;}),
        // marker: {
        //   size: Object.values(counts[k]).map((v,_)=>{return v.count*10;})
        // }
      });
      
    })
    return (traces);
  };

    
  return (
      <>
      <Toolbar>

        <AntTabs value={props.tabValue} onChange={props.handleTabChange} aria-label="ant example">
          <AntTab label={displayText.searchPage.tab1Text}/>
          <AntTab label={displayText.searchPage.tab2Text} />
          {/* <AntTab label="decision Type" /> */}
        </AntTabs>

        <Box style={{display:"block", position:"relative", marginLeft:16}}>
          {
            props.searchResults.length>0 &&
            <ExportCSV
              data={props.searchResults}
              searchQuery={props.inputValue}
            />
          }
        </Box>

        <Box style={{display:"block", position:"relative", marginLeft:16}}>
        {
          props.searchResults.length>0 &&
          <Tooltip title="Filter search results" >
            <IconButton edge="end" color="secondary" aria-label="filter" onClick={() => setShowFilters(!showFilters)} >
                < TuneIcon/>
            </IconButton>
          </Tooltip>
        }
        </Box>

        
      </Toolbar>

      

      <Container component="main" maxWidth="md" style={{display:"block", position:"absolute", marginLeft:"6%"}}>

        {
            props.searchResults.length>0 && showFilters &&
            <> 
              <Autocomplete
                multiple
                limitTags={1}
                size="small"
                id="combo-box-1"
                onSelect={(e)=>{setKommun(e.target.value)}}
                options={props.kommunList.sort((a, b) => -b.region.localeCompare(a.region))}
                groupBy={(option) => option.region}
                getOptionLabel={(option) => `${option.kommun}`}
                style={{ width: 250 , marginBottom:0}}
                renderInput={(params) => <CustomTextField {...params} label={displayText.searchPage.filterLabel} variant="outlined" />}
              />
            </>
        }
        {
          props.searchResults.length>0 &&
          <Typography variant="subtitle2"> {displayText.searchPage.searchResultStats.nDocsLabel}: {props.searchResults.length}, {displayText.searchPage.searchResultStats.nSentencesLabel}: {getSentenceCount(props.searchResults)} </Typography>
        }
        
        {
            props.loadingSearchResults &&
            <>
              <Skeleton height={10} style={{ marginBottom: 6, marginTop:16 }} />
              <Skeleton height={10} width="60%" style={{ marginBottom: 12}} />

              <Skeleton height={10} style={{ marginBottom: 6 }} />
              <Skeleton height={10} width="60%" style={{ marginBottom: 12}} />

              <Skeleton height={10} style={{ marginBottom: 6 }} />
              <Skeleton height={10} width="60%" />
            </>

        }

        {
          props.tabValue===0 && !props.loadingSearchResults && props.searchResults.length>0 &&
          <ItemList
            items={props.searchResults}
            search_query={props.inputValue}
            kommunList={props.kommunList}
            onListItemClick={(item)=>props.onListItemClick(item)}
            classifyIconClick={(text) => props.classifyIconClick(text)}
          /> 
        }
         

        {
          props.tabValue===1 && !props.loadingSearchResults && props.searchResults.length>0 &&
          <>
            <Plot
              data={ getData(props.searchResults)}
              layout={ {
                height: 600,
                width:1000,
                barmode: 'stack', 
                title: displayText.searchPage.docStatsBarChart.title, 
                yaxis: {title: displayText.searchPage.docStatsBarChart.yAxisLabel }, 
                xaxis:{
                  type:'date',
                  autorange: false,
                  range: getDateRange(12),
                  rangeselector: {buttons: [
                    {
                      count: 6,
                      label: '6m',
                      step: 'month',
                      stepmode: 'backward'
                    },
                    
                    {
                      count: 12,
                      label: '12m',
                      step: 'month',
                      stepmode: 'backward'
                    },
                    {
                      step: 'all', 
                    },
                    
                  ]},
              
                } 
              }}
              
            />

          </>
        }


        
      </Container>

    </>
  );


});

export default SearchResults;