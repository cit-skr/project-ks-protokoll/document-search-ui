import React, {memo, useState} from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import {
    TextField,
    Collapse,
    List,
    ListItem,
    Checkbox,
    IconButton,
    ListItemText,
    ListItemSecondaryAction,
    Typography,
    Badge,
    Tooltip
  } from '@material-ui/core';
import LibraryAddIcon from '@material-ui/icons/LibraryAdd';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AssessmentIcon from '@material-ui/icons/Assessment';
import AssignmentIcon from '@material-ui/icons/Assignment';

import AddSentenceButton from './AddSentenceButton'
import { useEffect } from 'react';

const prettyBytes = require('pretty-bytes');


const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 345,
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    }
}));

var parse = require('html-react-parser');



const SubListItem = memo((props) => {
    
    return (
        <ListItem divider={props.divider} style={{margin:6}}>
            <Typography style={{maxWidth:"70%"}} > {parse(props.text)}</Typography>
            <ListItemSecondaryAction style={{marginRight: 16}}>
                <Tooltip title="Classify this sentence">
                    <IconButton aria-label="classify" edge="end" onClick={() => props.classifyIconClick(props.text)}>
                        <AssessmentIcon/>
                    </IconButton>
                </Tooltip>
                <AddSentenceButton 
                    sentence={props.text}
                    search_query={props.search_query}
                    file_id={props.file_id}
                    municipality_id={props.municipality_id}
                />
            </ListItemSecondaryAction>
        </ListItem>
    );

});

const Item = memo((props) => {
    const classes = useStyles();
    const [expanded, setExpanded] = useState(false);

    useEffect(() => {
        setExpanded(false);
    }, [props]);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    const search = (id, data) => {
        for (var i=0; i < data.length; i++) {
            if (data[i].id === id) {
                return data[i];
            }
        }
    }

    const getTitle = (id, data, date, path) => {
        // const item = search(id,data)
        return path.replace("/"," - ") + " - " + String(date)
    }

    return (
    <div>
        <ListItem key={props.key} divider={props.divider} button onClick={()=> props.onListItemClick(props.url)}>
            <ListItemText primary={getTitle(props.municipality_id,props.kommunList,props.date, props.path)} secondary={"size: " + prettyBytes(props.size)+", pages: "+props.pageCount}/>
            <ListItemSecondaryAction>
                <Badge badgeContent={props.items.length} color="secondary" style={{marginRight:8}}>
                    <AssignmentIcon/>
                </Badge>
                <IconButton
                    className={clsx(classes.expand, {
                        [classes.expandOpen]: expanded,
                    })}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="expand"
                    edge="end"
                >
                    <ExpandMoreIcon />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
            <List dense>
                {props.items.map((item, indx) => (
                    <SubListItem
                        key={indx}
                        text={item}
                        classifyIconClick={props.classifyIconClick}
                        file_id={props.file_id}
                        municipality_id={props.municipality_id}
                        search_query={props.search_query}
                        divider={indx !== props.items.length - 1}
                    />
                ))}
            </List>
        </Collapse>
    </div>
    );
});

export default Item;
