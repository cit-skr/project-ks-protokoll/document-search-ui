import React from 'react'
import { Button, IconButton, Tooltip } from "@material-ui/core"
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';

const getDate = () => {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = yyyy + '-' + mm + '-' + dd 
    return today
}

const flatten = (data, q) => {
    var flattened =  []
    data.forEach((result, index) => {
        var hits = result.items;
        hits.forEach((xs,ix)=>{
            var ins = {items: xs, index: ix, query: q}
            flattened.push({...result, ...ins})
        })

    })
    return flattened

}


export const ExportCSV = ({data, searchQuery}) => {


    var fileName = "Search Reasults -- " + getDate() + " -- " + searchQuery;

    var cleaned_data = flatten(data, searchQuery)

    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const fileExtension = '.xlsx';

    const exportToCSV = (cleaned_data, fileName) => {
        const ws = XLSX.utils.json_to_sheet(cleaned_data);
        const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] };
        const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
        const data = new Blob([excelBuffer], {type: fileType});
        FileSaver.saveAs(data, fileName + fileExtension);
    }

    return (
        <Tooltip title="Download results to excel" >
            <IconButton edge="end" color="secondary" aria-label="logout" onClick={(e) => exportToCSV(cleaned_data,fileName)}>
                < CloudDownloadIcon/>
            </IconButton>
        </Tooltip>
    )
}