import React, {memo, useState} from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {Drawer, Divider,CssBaseline, Container, Typography, Grid, Toolbar, AppBar, IconButton, Button, List, ListItem, ListItemIcon, ß, Avatar, Paper} from "@material-ui/core"
import MenuIcon from '@material-ui/icons/Menu';
import SearchAutoComplete from "./SearchAutoComplete"
import Navbar from "./Navbar"

import {displayText} from "../common/config.json"
const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(16),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyItems: "center"
    },
    root: {
        display: 'flex',
        '& > *': {
          margin: theme.spacing(1),
        },
      },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
    large: {
        width: theme.spacing(8),
        height: theme.spacing(8),
    },
  }));



const LandingPage = memo((props) => {
    const classes = useStyles();
  
    return (
    <div >
        <Navbar 
            inputValue={props.inputValue}
            options={props.options}
            onInputKeyPress={e => props.onKeyPress(e)}
            searchBarDisplay='none'
            avatarDisplay='none'
            logout={props.logout}
            handleOpenSettingsDialog = {props.handleOpenSettingsDialog}
        />
        <Container component="main" maxWidth="sm" className={classes.paper}>
            <Grid container justify="center" style={{textAlign:"center"}}>
                <Grid item xs={"auto"} >
                    <Avatar alt="Document Seach" variant="square" src="./text.png" className={classes.large} />
                </Grid>
                    
                <Grid item xs={12} style={{alignItems:"center"}} >
                    <Typography variant="h4" style={{paddingBottom:16}}> {displayText.searchPage.title} </Typography>
                        <SearchAutoComplete
                            inputValue={props.inputValue}
                            options={props.options}
                            onInputKeyPress={e => props.onKeyPress(e)}
                        />
                </Grid>
            </Grid>
        </Container>
    </div>
    );
});


export default LandingPage