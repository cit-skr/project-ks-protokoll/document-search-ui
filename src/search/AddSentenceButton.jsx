import React, {memo, useState, useEffect} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import LibraryAddIcon from '@material-ui/icons/LibraryAdd';
import { IconButton, Typography, Menu, MenuItem, Tooltip, Box,Container, Popover } from '@material-ui/core'
import { TextField, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core'
import LabelIcon from '@material-ui/icons/Label';
import AddIcon from '@material-ui/icons/Add';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import MuiAlert from '@material-ui/lab/Alert';

import { gql, useMutation, useQuery } from '@apollo/client';
import { INSERT_SENTENCE, INSERT_LABEL, GET_LABELS } from "../common/HasuraQueries"

import {displayText} from '../common/config.json'

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const popUpMessageDefault = {
  "state":false,
  "severity":"", // severity list : "success", "error", "warning", "info"
  "message":""
}

const AddSentenceButton = memo((props) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [insertSentence] = useMutation(INSERT_SENTENCE);
  const [insertLabel] = useMutation(INSERT_LABEL)
  const [openPopUpMessage,setOpenPopUpMessage] = useState(popUpMessageDefault);
  const [openAddClassDialog, setOpenAddClassDialog] = useState(false);
  const [dialogInput, setDialogInput] = useState("");
  const { loading, error, data } = useQuery(GET_LABELS);
  const [labelList, setLabelList] = useState([]);


  function loadClasses() {
    if (!loading && data) {
      const j = data.ks_protokoll_schema_sentence_labels.map((item,_)=> {
        return {
          id:item.id,
          label:item.label,
        };
      });
      setLabelList(j);
    }
  }

  useEffect( ()=>{
    loadClasses();
  }, [loading, data]);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleAddClass =() => {
    setAnchorEl(null);
    setOpenAddClassDialog(true);
  };

  const handleDialogClose = () => {
    setOpenAddClassDialog(false);
  };
  
  const handleDialogAdd = () => {
    if (dialogInput.length>2) {
      var response = insertLabel({variables:{
        label: dialogInput
      }});
      response.then(result=>{
        setOpenPopUpMessage({state:true, severity: "success", message: "Added label: "+dialogInput})
        loadClasses();
        setDialogInput("")
      }).catch((result)=>{
        setOpenPopUpMessage({state:true, severity: "error", message:String(result)})
      });
    }
    setOpenAddClassDialog(false);
  };


  function handleAddSentence(label,label_id) {
    setAnchorEl(null);
    console.log(props)
    var response = insertSentence({variables:{
      label: label,
      label_id: label_id,
      text: props.sentence, 
      file_id: props.file_id? props.file_id: 0, 
      municipality_id: props.municipality_id? props.municipality_id: 0,
      search_query: props.search_query? props.search_query : ""
    }});
    response.then(result=>{
      var item = result.data.insert_ks_protokoll_schema_extracted_sentences_one;
      // console.log(result)
      setOpenPopUpMessage({state:true, severity: "success", message: "Added label: "+item.label})
      // return item;
    }).catch((result)=>{
      // console.log(result)
      setOpenPopUpMessage({state:true, severity: "error", message:String(result)})
    });
  }

  function handleClosePopUp() {
    setOpenPopUpMessage(popUpMessageDefault)
  }

  // function handleListKeyDown(event) {
  //   if (event.key === 'Tab') {
  //     event.preventDefault();
  //     setOpen(false);
  //   }
  // }

  return (
      <>
        <Tooltip title={displayText.classifierPage.AddSentenceLabel}>
          <IconButton aria-label="add-to-sentence-list" edge="end" onClick={handleClick} aria-controls="customized-menu" aria-haspopup="true" variant="contained">
              <LibraryAddIcon/>
          </IconButton>
        </Tooltip>

        <StyledMenu
          id="customized-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          {labelList.map( (val, index) => (
            <MenuItem onClick={() => handleAddSentence(val.label, val.id)} key={index} >
              <ListItemIcon>
                <LabelIcon fontSize="small"/>
              </ListItemIcon>
              <Typography variant="inherit"> {val.label} </Typography>
            </MenuItem>
          ))}
          <MenuItem onClick={handleAddClass} > 
            <ListItemIcon>
                <AddIcon fontSize="small"/>
            </ListItemIcon>
            <Typography variant="inherit"> {displayText.searchPage.addSentenceDialogueAddLabel} </Typography>
          </MenuItem>
      
        </StyledMenu>

        <Popover anchorReference="anchorPosition" anchorPosition={{ top: 900, left: 700 }}
          anchorOrigin={{
            vertical: 'center',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          id="popover status"
          open={openPopUpMessage.state}
          onClose={handleClosePopUp}
        >
          <Alert onClose={handleClosePopUp} severity={openPopUpMessage.severity}> 
            <Typography variant="inherit" > {openPopUpMessage.message} </Typography>
          </Alert>
        </Popover>

        <Dialog open={openAddClassDialog} onClose={handleDialogClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Add Class</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Add an additonal label for the sentences.
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Class"
              type=""
              value={dialogInput} 
              onChange={(e) => setDialogInput(e.target.value)} 
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleDialogClose} color="primary">
              Cancel
            </Button>
            <Button onClick={handleDialogAdd} color="primary">
              Add
            </Button>
          </DialogActions>
        </Dialog>
      </>
  );
});

export default AddSentenceButton;









