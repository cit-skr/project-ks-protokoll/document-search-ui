import {Box, Avatar, Drawer, Divider,CssBaseline, Container, Typography, Grid, Toolbar, AppBar, IconButton, Button, List, ListItem, ListItemIcon, ListItemText, Tooltip} from "@material-ui/core"
import clsx from 'clsx';
import { makeStyles, useTheme, withStyles } from '@material-ui/core/styles';
import React, { memo }from 'react';
import { Autocomplete }from '@material-ui/lab';
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import MenuIcon from '@material-ui/icons/Menu';
import history from "../history";
import SettingsIcon from '@material-ui/icons/Settings';
import FolderIcon from '@material-ui/icons/Folder';
import AssessmentIcon from '@material-ui/icons/Assessment';


import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

import SearchAutoComplete from "./SearchAutoComplete";

import {displayText} from "../common/config.json"

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: "relative",
    width:"70%"
  },
  small: {
      width: theme.spacing(4),
      height: theme.spacing(4),
  },
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  }
}));


const PersistentDrawer = memo((props)=>{
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={props.open}
        onClick={props.handleDrawerClose}
        onMouseLeave={props.handleDrawerClose}
        classes={{
        paper: classes.drawerPaper,
        }}
    >
        <div className={classes.drawerHeader}>
        <IconButton onClick={props.handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
        </IconButton>
        </div>
        <Divider />
        <List>
            <ListItem button key={"Search"} onClick={()=>{history.push("/")}}>
              <ListItemIcon> <SearchIcon/> </ListItemIcon>
              <ListItemText primary={displayText.navbar.searchLabel} />
            </ListItem>

            <ListItem button key={"File manager"} onClick={()=>{history.push("/fileManager")}}>
              <ListItemIcon> <FolderIcon/> </ListItemIcon>
              <ListItemText primary={displayText.navbar.fileManagerLabel} />
            </ListItem>

            <ListItem button key={"Classifier"} onClick={()=>{history.push("/classifier")}}>
              <ListItemIcon> <AssessmentIcon/> </ListItemIcon>
              <ListItemText primary={displayText.navbar.classifierLabel} />
            </ListItem>

        </List>
        <Divider />
        <List>
        
            <ListItem button key={"Settings"} onClick={ props.handleOpenSettingsDialog }>
            <ListItemIcon> <SettingsIcon/> </ListItemIcon>
            <ListItemText primary={displayText.navbar.settingsLabel} />
            </ListItem>

        </List>
    </Drawer>
  );



});

const Navbar = memo((props) => {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    return (
            <AppBar position="static" style={{background:"inherit"}} elevation={0}>
                <Toolbar style={{ display:"flex", alignContent:"center", flexShrink:"unset"}}>
                      <IconButton edge="start" color="secondary" size="medium" aria-label="menu" onClick={handleDrawerOpen}
                          className={classes.menuButton}
                      >
                        <MenuIcon />
                      </IconButton>
                      
                      <IconButton edge="start" color="secondary" size="medium" style={{ display: props.avatarDisplay, marginRight:8}} aria-label="menu" disableRipple onClick={()=>{history.push('/')}} >
                        <Avatar alt="Document Seach" variant="square" src="./text.png" className={classes.small} style={{marginLeft:16, marginRight:8}}/>
                        <Typography variant="h5" style={{color:"#222"}}> {displayText.searchPage.title} </Typography>
                      </IconButton>
                      <SearchAutoComplete
                        {...props}
                        width="70%"
                      />
                      <Divider component="div" flexItem variant="inset" />
                      <Box style={{display:"block", position:"absolute", marginLeft:"93%"}}>
                        <Tooltip title="Logout" >
                          <IconButton edge="end" color="secondary" aria-label="logout" onClick={props.logout}>
                            <ExitToAppIcon/>
                          </IconButton>
                        </Tooltip>
                      </Box>
                     
                </Toolbar>
                <PersistentDrawer
                    open={open}
                    handleOpenSettingsDialog = {props.handleOpenSettingsDialog}
                    handleDrawerClose={handleDrawerClose}
                />
            </AppBar>
    )
});

export default Navbar


