import React, { memo } from 'react'
import TextField from '@material-ui/core/TextField';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { Paper, Typography, FormControl, Select, InputLabel, Grid, FormControlLabel, Radio, RadioGroup, IconButton, Avatar} from '@material-ui/core';
import { Autocomplete }from '@material-ui/lab';
import SearchIcon from '@material-ui/icons/Search';
import MenuIcon from '@material-ui/icons/Menu';
import Divider from '@material-ui/core/Divider';
import InputBase from '@material-ui/core/InputBase';
import InputAdornment from '@material-ui/core/InputAdornment';



const CustomTextField = withStyles({
    root: {
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderRadius: 25,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          margin: 6
            
        },
      },
    },
  })(TextField);



const SearchAutoComplete = memo((props) => {
    return (
      <Autocomplete
          value={props.inputValue}
          onKeyPress={props.onInputKeyPress}
          freeSolo
          style={{display: props.searchBarDisplay? props.searchBarDisplay:"flex", margin:6, width:props.width?props.width:"100%", position:"relative"}}
          id="free-solo-2-search-bar"
          disableClearable
          options={props.options}
          renderInput={(params) => (
          
              <CustomTextField
                  {...params}
                  margin="none"
                  color="secondary"
                  variant="outlined"
                  style={{flexShrink:"initial"}}
                  // fullwidth
                  InputProps={
                      { 
                          ...params.InputProps, type: 'search',
                          startAdornment: (
                              <InputAdornment position="start" style={{paddingLeft:8, paddingTop: 2}} >
                                  <SearchIcon fontSize="large"/>
                              </InputAdornment>
                      
                          ),
                      }
                  }
              />

          
  
          )}
      />
        

    );
});

export default SearchAutoComplete;
