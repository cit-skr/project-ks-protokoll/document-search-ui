import React, {memo} from 'react';
import { List, Paper, Typography } from '@material-ui/core';
import Item from './Item'

const ItemList = memo((props) => {
    if (props.items.length>0 ){
        return (
            <Paper style={{ margin: 16 }} elevation={0} color="transparent">
                <List >
                {props.items.map((item, indx) => (
                    <Item
                        key={indx}
                        search_query={props.search_query}
                        url={item.url}
                        title={item.title}
                        pageCount={item.page_count}
                        size={item.size}
                        date={item.date}
                        file_id={item.file_id}
                        municipality_id={item.municipality_id}
                        path={item.path}
                        kommunList={props.kommunList}
                        items={item.items}
                        onListItemClick={props.onListItemClick}
                        divider={indx !== props.items.length - 1}
                        classifyIconClick={props.classifyIconClick}
                    />
                ))}
                </List>
            </Paper>
        );
    } else {
        return ( 
            <Typography align="left" style={{margin:16, justifyContent:"flex-start"}}>
                No search results found!
            </Typography>
         );
    }
    
});

export default ItemList;

