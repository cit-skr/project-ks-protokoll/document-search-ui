import React, {memo, useState} from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Paper, Grid, TextField, Button, Avatar,Typography, Container, FormControlLabel, CssBaseline, Checkbox  } from '@material-ui/core';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import {displayText} from '../common/config.json'

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
        width: theme.spacing(8),
        height: theme.spacing(8),
    },
    form: {
      width: '100%', 
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    }
  }));

const LoginPage = memo((props) => {
    const [password, setPassword] = useState("");
    const [rememberPass, setRememberPass] = useState(false);

    const classes = useStyles();

    const authenticate = () => {
        if (password === "cloud") {
            props.login(rememberPass);
        }
    };

    return (

        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
            <Avatar className={classes.avatar} alt="Document Search" variant="square" src="./text.png" />
            <Typography component="h1" variant="h5">
                {displayText.loginPage.title}
            </Typography>
            <form className={classes.form} noValidate>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label={displayText.loginPage.passwordLabel}
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    value={password} 
                    onChange={(e) => setPassword(e.target.value)} 
                    autoFocus
                />
                <FormControlLabel
                    control={<Checkbox value="remember" color="primary" onChange={()=>setRememberPass(!rememberPass)} />}
                    label={displayText.loginPage.stayLoggedIn}
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="secondary"
                    className={classes.submit}
                    onClick={authenticate} 
                    startIcon={<LockOpenIcon/>}
                >
                    {displayText.loginPage.loginButtonText}
                </Button>
            </form>
        </div>
    </Container>

    );
});

export default LoginPage;