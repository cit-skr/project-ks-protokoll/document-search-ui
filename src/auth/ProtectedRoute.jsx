import React , {memo, useState}from 'react'
import { Redirect } from 'react-router-dom'
import { useCookies, Cookies} from 'react-cookie';

const ProtectedRoute = memo((props) => {

    const cookies = new Cookies();

    const isAuthenticated = props.isAuthenticated ? true: (cookies.get('authenticated') === "yes") ? true : false;
    const component = props.component

    return isAuthenticated ? (
            component
        ) : (
            <Redirect to={{ pathname: '/login' }} />
    );

});

export default ProtectedRoute;